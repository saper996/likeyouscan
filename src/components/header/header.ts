import { Component, Input} from '@angular/core';
import { NavController} from 'ionic-angular';
import { Api } from '../../providers/api';
import { My_functions } from '../../providers/function/functions';
import { StartingPage } from '../../pages/starting/starting';

/**
 * Generated class for the HeaderComponent component.
 *
 * See https://angular.io/docs/ts/latest/api/core/index/ComponentMetadata-class.html
 * for more info on Angular Components.
 */
@Component({
  selector: 'standart-navbar',
  templateUrl: 'header.html'
})
export class HeaderComponent {
  @Input() title;
  @Input() type:ColorHeader;
  public enum=ColorHeader;
  icon:string;
  flag:boolean;
  constructor(public navctr:NavController, private myfunc:My_functions, private api:Api) {
  }
  ngOnInit(){
    if(this.type == null)
      this.type=ColorHeader.purple;
    this.flag=this.api.token != null;
  }
  back(){
      this.navctr.pop();
  }
  exit(){
    this.api.seller.user.logout().then(value =>{
      this.myfunc.alert4create({
        message:value.data.messages[0],
      });
      if(value.status)
        this.navctr.setRoot(StartingPage);
    }).catch(error =>{
      console.log(error);
      this.myfunc.alert4create();
    });
  }
}
/**
 * Цветовой тип шапки
 */
export enum ColorHeader{
  white, purple
}
