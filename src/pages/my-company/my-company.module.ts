import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyCompanyPage } from './my-company';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    MyCompanyPage,
  ],
  imports: [
    IonicPageModule.forChild(MyCompanyPage),
    ComponentsModule
  ],
})
export class MyCompanyPageModule {}
