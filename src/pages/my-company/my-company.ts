import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, AlertController } from 'ionic-angular';
import { AddCompanyPage } from '../add-company/add-company';
import { Api } from '../../providers/api';
import { Company } from '../../public_classes/data/data';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-my-company',
  templateUrl: 'my-company.html',
})
export class MyCompanyPage {
  company:Company[];
  constructor(public navCtrl: NavController, public navParams: NavParams,private view:ViewController,private api:Api,private alertCtrl:AlertController) {
  }
  ionViewDidEnter(){
    let me=this;
    this.api.seller.company.get().then(value=>{
      if(value.status){
        me.company= value.data;
      }
      else{
        let alert= this.alertCtrl.create({
          title: 'Ошибка компаний',
          message: value['data']['messages'],
          buttons: ['ОК'],
          mode:'ios',
          cssClass: 'alert4 alert4_head'
        });
        alert.present();
      }
    }).catch(error =>{
      throw error;
    });
  }

  trans(){
   this.navCtrl.push(AddCompanyPage);
  }


  sellectCompany(item:Company){
    this.api.seller.company.sellect(item.id_company).then(value=>{
      if(value.status){
        this.navCtrl.push(HomePage);
      }
      else{
        let alert= this.alertCtrl.create({
          title:'Ошибка',
          message:value.data.messages[0],
          buttons:['OK'],
          mode:'ios',
          cssClass: 'alert4 alert4_head'
        });
        alert.present();
      }
    }).catch((value) =>{
      console.log(value);
      let alert= this.alertCtrl.create({
        message: 'Упс, что-то пошло не так',
        buttons: ['ОК'],
        mode:'ios',
        cssClass: 'alert4'
      });
      alert.present();
    });
  }
  del_company(item:Company){
    
  }
}