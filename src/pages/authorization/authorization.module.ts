import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AuthorizationPage } from './authorization';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AuthorizationPage,
  ],
  imports: [
    IonicPageModule.forChild(AuthorizationPage),
    ComponentsModule
  ],
})
export class AuthorizationPageModule {}
