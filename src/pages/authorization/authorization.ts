import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform, LoadingController, AlertController } from 'ionic-angular';
import { Api } from '../../providers/api';
import { My_functions } from '../../providers/function/functions';
import { MyCompanyPage } from '../my-company/my-company';

@IonicPage()
@Component({
  selector: 'page-authorization',
  templateUrl: 'authorization.html',
})
export class AuthorizationPage {
  username:string;
  password:string;
  load:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,private api:Api,private main:Api,private platform:Platform,private myfunc:My_functions,public loadctr:LoadingController,public alertCtrl:AlertController) {
  }
  Login(){
    // Проверки
    if(this.username == null || this.username =='')
    {
      this.myfunc.toastmenu('Пустой логин', 5000, 'top');
      return;
    }
    if(this.password == null || this.password =='')
    {
      this.myfunc.toastmenu('Пустой пароль', 5000, 'top');
      return;
    }
    //---- Конец проверок

    this.load=this.loadctr.create({
      content: 'Соединение с сервером',
      cssClass: 'loading'
    });
    this.load.present();
    this.api.public.seller.login(this.username,this.password).then((value)=>{
      if(value['status']==true){
        this.navCtrl.setRoot(MyCompanyPage);
        this.load.dismiss();
      }
      else{
      this.load.dismiss();
        let alert = this.alertCtrl.create({
          title: 'Авторизация неудалась',
          subTitle: value['data']['messages'],
          buttons: ['ОК']
    });
    alert.present();
    }
  })
  }
}
