import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddCompanyPage } from './add-company';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    AddCompanyPage,
  ],
  imports: [
    IonicPageModule.forChild(AddCompanyPage),
    ComponentsModule
  ],
})
export class AddCompanyPageModule {}
