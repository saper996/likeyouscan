import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Api } from '../../providers/api';

@IonicPage()
@Component({
  selector: 'page-add-company',
  templateUrl: 'add-company.html',
})
export class AddCompanyPage {
  companyKey:string;
  constructor(public navCtrl: NavController, public navParams: NavParams,private api:Api,private alertCtrl:AlertController) {
  }
  addCompany(){
    this.api.seller.company.requestbycode(this.companyKey).then(value=>{
      if(value.status){
        let alert= this.alertCtrl.create({
          message: 'Компания созданна',
          buttons: ['ОК'],
          mode:'ios',
          cssClass: 'alert4'
        });
        this.navCtrl.pop();
      }
      else{
        let alert= this.alertCtrl.create({
          title: 'Ошибка добавления компании',
          message: value['data']['messages'],
          buttons: ['ОК'],
          mode:'ios',
          cssClass: 'alert4 alert4_head'
        });
        alert.present();
      }
    }).catch((value) =>{
      console.log(value);
      let alert= this.alertCtrl.create({
        message: 'Упс, что-то пошло не так',
        buttons: ['ОК'],
        mode:'ios',
        cssClass: 'alert4'
      });
      alert.present();
    });

  }

}
