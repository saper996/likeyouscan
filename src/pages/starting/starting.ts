import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RegistrationPage } from '../registration/registration';
import { AuthorizationPage } from '../authorization/authorization';
import { LostPasPage } from '../lost-pas/lost-pas';
import { Api } from '../../providers/api';
import { MyCompanyPage } from '../my-company/my-company';
import { My_functions } from '../../providers/function/functions';

/**
 * Generated class for the StartingPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-starting',
  templateUrl: 'starting.html',
})
export class StartingPage {

  constructor(public navCtrl: NavController, private myfunc:My_functions, public navParams: NavParams,public api:Api) {
    this.api.public.seller.ping().then(value=>{
      if(value){
        this.navCtrl.setRoot(MyCompanyPage);
      }
      else{
      }
    });
  }
  pushPageAuthorization(){
    this.navCtrl.push(AuthorizationPage);
  }
  pushPageRegistration(){
    this.navCtrl.push(RegistrationPage);
  }
  pushPageLostPas(){
    this.navCtrl.push(LostPasPage);
  }
}
