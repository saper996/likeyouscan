import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { StartingPage } from './starting';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    StartingPage,
  ],
  imports: [
    IonicPageModule.forChild(StartingPage),
    ComponentsModule
  ],
})
export class StartingPageModule {}
