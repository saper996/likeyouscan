import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { My_functions } from '../../providers/function/functions';
import { FormGroup } from '../../../node_modules/@angular/forms';
import { MyValidators } from '../../public_classes/data/validators';
import { Api } from '../../providers/api';
import { Restore_Obj, TypesHelp } from '../popover-help/popover-help';

/**
 * Generated class for the LostPasPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-lost-pas',
  templateUrl: 'lost-pas.html',
})
export class LostPasPage {
  role:number;
  subpage:number;
  control:FormGroup;
  username:string;
  email:string;
  phone:number;

  constructor(public navCtrl: NavController, public navParams: NavParams, public my_func:My_functions, public alertCtrl:AlertController, public api:Api) {
    this.role=this.navParams.data.role;
    if(navParams.data.role == null){
      this.subpage = 0;
    }
    else{
      this.role=navParams.data.role;
      this.control=new FormGroup({
        email: MyValidators.emailvalidator(),
        phone: MyValidators.mobilevalidator(),
      });
      }
    }
    restore_pas(){
      let me=this;
      if(this.role == 0){
        this.api.public.seller.restore_pas_email(this.email).then(value =>{
          console.log(value);
          if(value.status){
            this.my_func.toastmenu('код отправлен',3000,'middle');
            let temp:Restore_Obj={
              type:null,
              func:{
                callbackfail:(obj:any)=>{
                  me.my_func.toastmenu('Сброс неудался', 3000, 'top');
                },
                callbacksucsess:(obj:any)=>{
                  me.my_func.toastmenu('Сброс успешен', 3000, 'top')
                  me.navCtrl.pop();
                },
              },
              user:this.username
            };
            this.my_func.helpPopover(TypesHelp.restore_pas, temp);
          } else{
            this.my_func.toastmenu(value.data.messages[0], 3000,'middle');
          }
        }).catch(error =>{
          console.log(error);
        });
      }else{
        this.api.public.seller.restore_pas_phone('8'+this.phone).then(value =>{
          console.log(value);
          if(value.status){
            this.my_func.toastmenu('код отправлен',3000,'middle');
            let temp:Restore_Obj={
              type:null,
              func:{
                callbackfail:(obj:any)=>{
                  me.my_func.toastmenu('Сброс неудался', 3000, 'top');
                },
                callbacksucsess:(obj:any)=>{
                  me.my_func.toastmenu('Сброс успешен', 3000, 'top')
                  me.navCtrl.pop();
                },
              },
              user:this.username
            };
            this.my_func.helpPopover(TypesHelp.restore_pas, temp);
          } else{
            this.my_func.toastmenu(value.data.messages[0], 3000,'middle');
          }
        }).catch(error =>{
          console.log(error);
        });
      }
    }
  

  restorenext(){
    if(this.role == null){
      let temp= this.my_func.alertCtrl.create({
        message:'Для продолжения выберите способ восстановления пароля  ',
        mode:'ios',
        buttons:[
          {
            text:'Закрыть',
            role:'close'
          }
        ],
        cssClass:'alert4'
      });
      temp.present();
    }
    else
    this.navCtrl.push(LostPasPage, {role:this.role});
  }
}