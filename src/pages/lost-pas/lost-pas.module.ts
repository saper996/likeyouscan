import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { LostPasPage } from './lost-pas';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  declarations: [
    LostPasPage,
  ],
  imports: [
    IonicPageModule.forChild(LostPasPage),
    ComponentsModule
  ],
  exports: [
    LostPasPage
  ]
})
export class LostPasPageModule {}
