import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, Events } from 'ionic-angular';
import { My_functions } from '../../providers/function/functions';
import { Api } from '../../providers/api';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { FormGroup, FormControl, ValidatorFn, AbstractControl } from '../../../node_modules/@angular/forms';
import { MyCompanyPage } from '../my-company/my-company';
import { MyValidators } from '../../public_classes/data/validators';

@IonicPage()
@Component({
  selector: 'page-registration',
  templateUrl: 'registration.html',
})
export class RegistrationPage {
  username:string;
  password1:string;
  FIO:string;
  password2:string;
  email:string;
  phone:string;
  control:FormGroup;
  type:string='data';
  flag_button_registration:boolean=true;
  private validatorpas:ValidatorFn=(control:AbstractControl)=>{
    if(this.control != null)
    return control.value === this.control.controls['password'].value ? null : { 'passowrd not round': true};
    else
      return null;
  }
  constructor(public navCtrl: NavController, public navParams: NavParams,public my_func:My_functions,public alertCtrl:AlertController,public api:Api,public events:Events,private iab:InAppBrowser) {
    this.control=new FormGroup({
      login: MyValidators.loginvalidator(),
      email: MyValidators.emailvalidator(),
      password: MyValidators.passwordvalidator(),
      phone: MyValidators.mobilevalidator(),
      FIO: MyValidators.FIOvalidator(),
      requestpassword: new FormControl('', this.validatorpas)
    });
  }
  
  Registration(){
    if(this.FIO == null || this.FIO =='')
    {
      this.my_func.toastmenu('ФИО пользователя пустое', 5000, 'top');
      this.type='data';
      return;
    }
    if(this.email == null || this.email =='')
    {
      this.my_func.toastmenu('Email пустой', 5000, 'top');
      this.type='data';
      return;
    }
    if(this.username == null || this.username =='')
    {
      this.my_func.toastmenu('Логин пользователя пустой', 5000, 'top');
      this.type='data';
      return;
    }
    if(this.password1 == null || this.password1 =='')
    {
      this.my_func.toastmenu('Пароль пустой', 5000, 'top');
      this.type='data';
      return;
    }
    if(this.password2 == null || this.password2 =='')
    {
      this.my_func.toastmenu('Подтверждение пароля пустое', 5000, 'top');
      this.type='data';
      return;
    }
    if(this.phone == null || this.phone=='')
    {
      this.my_func.toastmenu('Номер пользователя пустой', 5000, 'top');
      this.type='data';
      return;
    }
      if(this.password1 == this.password2)
      {
          if(this.flag_button_registration)
          {
            let me=this;
            me.flag_button_registration=false;
            setTimeout(function() {
              me.flag_button_registration=true;
            }, 5000);
            this.api.public.seller.registration(this.username,this.password1,this.email,this.FIO,this.phone).then((data)=>{
              this.my_func.toastclose();
              if(data['status']==true)
              {
                let alert = this.alertCtrl.create({
                  title: 'Регистрация завершена',
                  buttons: ['ОК']
                });
                alert.present();
                this.navCtrl.setRoot(MyCompanyPage);
              }
              else
              {
                let alert = this.alertCtrl.create({
                  title: 'Регистрация неудалась',
                  message: data['data']['messages'],
                  buttons: ['ОК'],
                  mode:'ios',
                  cssClass: 'alert4 head'
                });
                alert.present();
              }
            }).catch((data) =>{
              alert(data);
            });
          }
          else
            this.my_func.toastmenu('Пожалуйста подождите запрос обрабатывается', 5000,'top');
      }
      else
      {
        let alert = this.alertCtrl.create({
          title: 'Ошибка заполнения',
          subTitle: 'Пароль и подтверждение пароля не совпадают',
          buttons: ['ОК']
        });
        this.type='data';
        alert.present();
      }
  }

}
