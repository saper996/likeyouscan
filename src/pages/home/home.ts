import { Component, Testability } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { InfoPage } from '../info/info';
import { Api } from '../../providers/api';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  QRkey:string;
  constructor(public navCtrl: NavController, private navparam:NavParams, private scan:QRScanner, private camera:Camera, private per:Api,private alertCtrl:AlertController ) 
  {

  }
  QRscanner(){
    let me=this;
    this.scan.prepare().then((status: QRScannerStatus) => {
      if (status.authorized) 
      {
        console.log("scanning");
        let temp;
        let ionApp = <HTMLElement>document.getElementsByTagName("ion-app")[0];
        let scanSub = this.scan.scan().subscribe((QRkey) => {
          console.log('Scanned something', QRkey);
          this.scan.hide(); // hide camera preview
          scanSub.unsubscribe(); // stop scanning
          ionApp.style.display = "block";
          clearTimeout(temp);
          me.goinfo(QRkey);
        });
        ionApp.style.display = "none";
        this.scan.show();
        temp=setTimeout(() => {
          ionApp.style.display = "block";
          scanSub.unsubscribe(); // stop scann
          this.scan.hide();
        }, 50000);
      } else if (status.denied) {
       // camera permission was permanently denied
       // you must use QRScanner.openSettings() method to guide the user to the settings page
       // then they can grant the permission from there
      } else {
       // permission was denied, but not permanently. You can ask for permission again at a later time.
      }
    }).catch((e: any) => {
      console.log ('Error is', e); 
    })
  
  }
  goinfo(qr:string){
    let me=this;
    try{
      me.per.seller.promo.checkCode(qr).then(value=>{
        if(value.status){
          me.navCtrl.push(InfoPage,value.data);
        }
        else{
          let alert= me.alertCtrl.create({
            title: 'Ошибка Qrcode',
            message: value['data']['messages'],
            buttons: ['ОК'],
            mode:'ios',
            cssClass: 'alert4_head'
          });
          alert.present();
        }
      }).catch((value) =>{
          alert(value);
        });
    }
    catch(e){
      console.log("error=",e);
    }
  }
}
