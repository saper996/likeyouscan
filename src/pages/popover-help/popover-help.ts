import { Component } from '@angular/core';
import { IonicPage, ViewController, NavParams } from 'ionic-angular';
import { Api } from '../../providers/api';

/**
 * Generated class for the PopoverHelpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-popover-help',
  templateUrl: 'popover-help.html',
})
export class PopoverHelpPage {
  type:TypesHelp;
  Variable=TypesHelp;
  callbacksucsess:(obj:any)=>void;
  callbackfail:(obj:any)=>void;
  item:any;
  constructor(private viewcontroller: ViewController, private navparams:NavParams, private api:Api) {
    this.type=this.navparams.data.type;
  }
  ngOnInit() {
    if (this.navparams.data) 
      this.type=this.navparams.data.type;
    else
      this.type=TypesHelp.registration_status_help;
    //---- 
    if(this.type == TypesHelp.restore_pas){
      this.item=this.navparams.data;
    } else{
      this.item={}
    }
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad PopoverHelpPage');
  }
  close(){
    this.viewcontroller.dismiss();
  }
  restore(){
    let me=this;
    this.api.public.seller.restore_pass(this.item.code, this.item.passwod).then(value =>{
      if(value.status){
        me.item.func.callbacksucsess(value);
        me.viewcontroller.dismiss();
      }
      else
        me.item.func.callbackfail(value);
    });
  }
}
export enum TypesHelp{
  registration_status_help,
  executor_app_help,
  restore_pas,
  confrim_payout
};
export interface Restore_Obj{
  type?:TypesHelp;
  func:{
    callbacksucsess:(obj:any)=>void;
    callbackfail:(obj:any)=>void;
  }
  user:string;
  code?:string;
  passwod?:string;
}