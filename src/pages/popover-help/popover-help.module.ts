import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PopoverHelpPage } from './popover-help';

@NgModule({
  declarations: [
    PopoverHelpPage,
  ],
  imports: [
    IonicPageModule.forChild(PopoverHelpPage),
  ],
})
export class PopoverHelpPageModule {}
