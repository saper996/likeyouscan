import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { TaskPromo } from '../../public_classes/data/data';


@IonicPage()
@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
  promocod:TaskPromo;
  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.promocod=navParams.data;
  }


}
