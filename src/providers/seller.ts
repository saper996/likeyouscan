import { Api } from "./api";
import { AnswerDataArrayT, Company, AnswerDataFalse } from "../public_classes/data/data";

export class Seller{
    company:ProviderCompany;
    promo:Promo;
    user:User;
    constructor(main:Api) {
        this.company= new ProviderCompany(main);
        this.promo=new Promo(main);
        this.user=new User(main);
    }
}
class ProviderCompany{
    constructor(private main:Api) {
        
    }
    get():Promise<AnswerDataArrayT<Company>>{
        let url='Seller/Company/Get';
        return this.main.post(url,{}).then(value =>{
            return value;
        }).catch( error =>{
            throw error;
        });
    }
    requestbycode(code:string){
        let url='Seller/Company/RequestByCode'
        return this.main.post(url,{
            code:code
        }).then(value =>{
            return value;
        }).catch( error=>{
            throw error;
        });
    }
    //TODO: 
    sellect(idcompany:number):Promise<AnswerDataFalse>{
        return new Promise(resolve=>{
            if(idcompany == null){
                resolve({status:false, data:{messages:['Не отправлен ид']}});
            }
            else{
                resolve({status:true, data:null})
            }
        })
    }
}
class Promo{
    constructor(private main:Api) {
    }
    checkCode(code:string){
    let url='Seller/Promo/CheckCode';
    return this.main.post(url,{
        code:code
    }).then(value=>{
        return value;
    }).catch(error=>{
        throw error;
    })
    }
}
class User{
    constructor(private main:Api) {
        
    }
    logout():Promise<AnswerDataFalse>{
        let url='Seller/User/Logout';
        return this.main.post(url,{}).then(value =>{
            this.main.token=null;
            this.main.stor_set('token',null);
            return value;
        }).catch(error =>{
            throw error;
        })
    }
}