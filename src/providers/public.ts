import { Api } from "./api";
import { AnswerDataFalse, AnswerData } from "../public_classes/data/data";

export class Public{
    seller:Seller;
    constructor(private api:Api){
        this.seller= new Seller(api);
    }


}
class Seller{
    FIO:string;
    constructor(private api:Api){
    }
    ping():Promise<boolean>{
        let me=this;
        let url='Seller/User/Ping';
        if(!me.api.token){
            return me.api.stor_get('token').then(value=>{
               if(value != null){
                    me.api.token=value;
                    return me.api.post(url,'').then(value=>{
                        if(!value.status){
                            me.api.token=null;
                            me.api.stor_set('token', null);
                        }
                        return value.status;
                    })
                }
                else{
                    return false;
                }
            
            })
        }
        else{
            
        }
    }
    registration(login:string,password:string,email:string,FIO:string,phone:string){
        let url='Public/Seller/Register';
        return this.api.post(url,{login:login, password:password, fullName:FIO,email:email,phone:phone}).then( value =>{
            if(value['status']){
                this.api.token=value.data.token;
                this.api.stor_set('token', value.data.token);
            }
            return value;
        }).catch( error =>{
            throw error;
        });
    }
    login(login:string, password:string){
        let url='Public/Seller/Login';
        return this.api.post(url,{login:login, password:password, }).then( value =>{
            if(value['status']){
            this.api.token=value.data.token;
            this.api.stor_set('token', value.data.token);
            }
            return value;
        }).catch( error =>{
            throw error;
        });
    }
    restore_pas_email(email:string):Promise<AnswerDataFalse>{
        let url=`Public/Seller/RequestRestoreCodeByEmail`;
        return this.api.post(url,{email:email}).then(value =>{
            return value;
        }).catch(error =>{
            return error;
        })
    }
    restore_pas_phone(phone:string):Promise<AnswerData>{
        let url=`Public/Seller/RequestRestoreCodeByPhone`;
        return this.api.post(url,{ phone:phone}).then(value =>{
            return value;
        }).catch(error =>{
            return error;
        })
    }
    restore_pass(code:string, new_password:string):Promise<AnswerData>{
        let url=`Public/Seller/RestoreByCode`;
        return this.api.post(url, {newPassword:new_password, code:code}).then(value =>{
            return value;
        }).catch(error =>{
            return error;
        });
    }
}