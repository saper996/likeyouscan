import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';
import { Deeplinks } from '@ionic-native/deeplinks';
import { AlertController, Platform, App } from 'ionic-angular';
import { PushNotificaton } from './push-notificaton';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { OneSignal } from '@ionic-native/onesignal';
import { Http, Headers } from '@angular/http';
import { Network } from '@ionic-native/network';
import { FileTransfer, FileTransferObject, FileUploadOptions, FileUploadResult } from '@ionic-native/file-transfer';
import { My_functions } from './function/functions';
import { AnswerData } from '../public_classes/data/data';
import { File, FileEntry } from '@ionic-native/file';
import { Public } from './public';
import { Seller } from './seller';

@Injectable()
export class Api {
  token:string;
  cities:any;
  page:any;
  push: PushNotificaton;
  socket:WebSocket;
  number_file:number;
  public:Public;
  seller:Seller;
  private message_false={"status":false, "data":{"messages":'нет соединения'}};
  public BaseUrl:string ='https://api.likeyou.info/v4_2/';
  constructor( private http:Http, private network: Network,  alert:AlertController, private file:File,
  private stor:Storage, private platform:Platform, oneSignal:OneSignal, myfunc:My_functions,
  app:App, private filtetransfer:FileTransfer, iab: InAppBrowser , deep:Deeplinks) {
    this.number_file=0;
    this.public=new Public(this);
    this.seller=new Seller(this);
    this.seller.company.get
    //this.push=new PushNotificaton(platform, app, oneSignal, alert, this);
  }
  private createhead(){
    return new Headers({
      token:this.token
    });
  }
  get(url:string){
      return new Promise<AnswerData>(resolve=>{
        if(this.connectiontest())
          this.http.get(this.BaseUrl+url, { headers:this.createhead() })
            .subscribe(res=> 
            {
              //console.log(res);
              try{
                resolve(res.json());
              }
              catch(e){
                console.log('запрос',res);
                console.log('ошибка',e);
                resolve({status:false, data:{"messages":['незвестная ошибка']}});
              }
            },
            error=>{
              try{
                console.log(error.json());
                resolve({status:false, data:{"messages":['ошибка получения данных']}});
              }
              catch(e){
                console.log('error=', e);
                console.log(error);
                resolve({status:false, data:{"messages":['неизвестная ошибка']}});
              }
          });
        else{
          resolve({"status":false, "data":{"messages":['нет соединения']}});
        }
      }).catch<AnswerData>(error =>{
        return ({"status":false, data:{"messages":['незвестная ошибка']} });
      });
  }
  /**
   * Запрос типа пост
   * @param url нач адрес + переданный урл
   * @param data JSON объект
   */
  post(url:string, data:string):Promise<AnswerData>
  /**
   * Запрос типа пост
   * @param url нач адрес + переданный урл
   * @param data object автоматический преобразованный в JSON
   */
  post(url:string, data:object):Promise<AnswerData>
  /**
   * Запрос типа пост
   * @param url нач адрес + переданный урл
   * @param data object
   * @param convert преобразовывать в json или нет(true - преобразовать, false - нет, по умолчанию true)
   */
  post(url:string, data:object, convert):Promise<AnswerData>
  post(url:string, data:string | object, convert:boolean=true):Promise<AnswerData>{
    if(typeof data != 'string' && convert){
      data=JSON.stringify(data);
    }
    return new Promise<AnswerData>(resolve=>{
      if(this.connectiontest())
        this.http.post(this.BaseUrl+url,
        data, { headers:this.createhead() })
        .subscribe(res=>{ 
          try{
            //console.log( 'итог', { json:JSON.stringify({url: res.url, answer: res.json(), param: JSON.parse(data)}),
            //notjson:{url: res.url, answer: res.json(), param: JSON.parse(data)} });
            resolve(res.json());
          }
          catch(e){
            console.log('запрос',res);
            console.log('ошибка',e);
          }
        },
        error=>{
          try{
            //resolve(error.json());
            console.log('poster=',error.json());
          }
          catch(e){
            console.log('error=', e);
            console.log(error);
          }
          throw error;
        });
        else{
          resolve({"status":false, "data":{"messages":'нет соединения'}});
        }
      });
  }
  delete(url:string, obj:any){
    return new Promise<AnswerData>(resolve =>{ 
      if(this.connectiontest())
        this.http.delete(this.BaseUrl+url,
          {body: obj, headers:this.createhead()})
          .subscribe(res=>{ 
              try{
                resolve(res.json());
              }
              catch(e){
                console.log('запрос',res);
                console.log('ошибка',e);
              }
            },
            error=>{
              try{
                resolve(error.json());
                console.log(error.json());
              }
              catch(e){
                console.log('error=', e);
                console.log(error);
              }
            }
          );
        else{
          resolve(this.message_false);
        }
    });
  }
  stor_set(key:string,value:any){
    this.stor.set(key, value);
  }
  stor_get(key:string){
    return this.stor.get(key).then((val) => {
      return val;
    }).catch( error =>{
      return error;
    });
  }
  Upload(funcupload:(response:FileUploadResult) =>void, funcerror:(error:object)=>void, funcprogress:(progres:ProgressEvent)=>void, exifstr:any){
    let exif=exifstr;
    let size:number;
    let FileTransfer: FileTransferObject= this.filtetransfer.create();
    let fileopt:FileUploadOptions={
      headers:{
        token:this.token,
      }
    }
    this.file.resolveLocalFilesystemUrl(exif.filename).then( value =>{
      value.getMetadata( (sucses)=>{
          size=sucses.size;
          console.log(sucses.size);
        }, 
        (error)=>{
          console.log(error);
        }
      )
    }).catch( error =>{
      console.log('errorfile', error);
    })
    //console.log('dataupload_picture=', this.BaseUrl+`save_photo/${obj.lat}/${obj.lng}`, exif.json_metadata.gpsLatitude, exif.json_metadata.gpsLongitude);
    FileTransfer.upload(exif.filename,this.BaseUrl+`User/Image/Save`, fileopt)
    .then((value)=>{
      //console.log(value);
      funcupload(value);
    })
    .catch((error)=>{
      console.log(error);
      funcerror(error);
    });
    if(this.platform.is('ios'))
      FileTransfer.onProgress(progres =>{
        let temp:any=progres;
        temp.total=size;
        funcprogress(temp);
      });
    else
      FileTransfer.onProgress(funcprogress);
  }
  donwload(url):Promise<FileEntry>{
    let fileTransfer: FileTransferObject= this.filtetransfer.create();
    let me=this;
    return fileTransfer.download(url, this.file.dataDirectory +this.number_file + '.jpg').then((entry) => {
      console.log('download complete: ' + entry.toURL());
      me.number_file++;
      return entry;
    }, (error) => {
      console.log(error);
      throw error;
    });
  }
  /**
   * функция проверки соединения с инетом
   */
  connectiontest():boolean{
    if(!this.platform.is('cordova'))
      return true;
    if(this.network.type != 'none')
      return true;
    else
      return false;
  }
  
  /*websocet():Promise<WebSocket>{
    return new Promise<WebSocket>( resolve =>{
      if(this.socket == null)
        this.socket=new WebSocket(`wss://api-likeyou.net.ru/v4/socket.php:8080?token=${this.token}`)
        //new ssl
      else
        return this.socket;
      let me=this;
      this.socket.onopen=(ev) =>{
        console.log(ev);
        resolve(me.socket);
      }
      this.socket.onerror=(ev) =>{
        console.log(ev);
      }
    });
  }*/

}

