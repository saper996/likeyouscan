import { OneSignal, OSNotificationPayload } from '@ionic-native/onesignal';
import { App, AlertController, NavController } from 'ionic-angular';
import { Api } from './api';
import { Platform } from 'ionic-angular/';
import { AlertOptions } from 'ionic-angular/components/alert/alert-options';
import { AnswerData, AnswerDataFalse, AnswerDataPush, DataPush } from '../public_classes/data/data';

//import { MyExecutorTaskPage, MyTaskPage, ModhomePage, ModeratorTaskAcceptPage } from '../../pages/pages';
/*
  Generated class for the PushNotificatonProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
export enum pushid{
  /**
   * Пустой 0
   */
  empty,
  /**
   * Продавец принят
   */
  sellerAccepted,
  /**
   * Продавец откланён
   */
  sellerNotAccepted
}
export class PushNotificaton {
  private readonly appid:string='0c42c481-1a46-45c4-bd8b-37a600c56a8e';
  private readonly googlenumber:string='205747563016';
  constructor(private platform:Platform, private app:App, private oneSignal:OneSignal, private alert:AlertController, private api:Api) {
  }
  /**
   * инцициализировать push
   */
  init(){
    this.initpush();
  }
  private initpush(){
    console.log('push start');
    this.oneSignal.startInit(this.appid, this.googlenumber);
    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.None);
    this.oneSignal.handleNotificationReceived().subscribe(res => {
      console.log('message resive=',res);
      if(this.api.token != null)
        this.createalert(res.payload);
      // do something when notification is received
    });
      
    this.oneSignal.handleNotificationOpened().subscribe( res => {
      console.log('messageresive=',res);
      if(this.api.token != null)
        this.initsetpage(res.notification.payload);
      // do something when a notification is opened
    });
    this.oneSignal.endInit();
    console.log('push end');
  }
  /**
   * Создать алерт
   * @param res сообщение
   */
  private createalert(res: OSNotificationPayload){
    let me=this;
    let index:number=null;
    if(!this.platform.is('ios')){
      if(res.actionButtons != null && res.actionButtons.length > 0)
        index=parseInt(res.actionButtons[0].id);
      else
        index=0;
    }
    else{
      if(res.additionalData != null && res.additionalData.actionButtons.length >0)
        index=parseInt(res.additionalData.actionButtons[0].id);
      else
        index=0;
    }
    console.log('message=', res,  index);
    let temp:AlertOptions=null;
    switch(index){
      case pushid.sellerAccepted:{
        temp={
          title:res.title,
          message:res.body,
          cssClass:'appchange',
          buttons:[
            {
              text: 'Перейти в Компании',
              cssClass: 'changeapp',
              handler:()=>{
                me.setpage('MyExecutorTaskPage', null);
              }
            }
          ]
        }
        break;
      }
      case pushid.sellerNotAccepted:{
        temp={
          title:res.title,
          message:res.body,
          cssClass:'appchange',
          buttons:[
            {
              text: 'Ок',
              cssClass: 'changeapp',
              handler:()=>{
                me.setpage('MyExecutorTaskPage', null);
              }
            }
          ]
        }
        break;
      }
    }
    if(temp == null){
      temp={
        title:res.title,
        message:res.body,
        cssClass:'taskchange',
        buttons:[
          {
            text: 'закрыть',
            cssClass: 'changetask',
          }
        ]
      }
    }
    else
      temp.buttons.push({
      text: 'Ок',
      cssClass: 'cancel',
      role: 'cancel',
    });
    let alert=me.alert.create(temp);
    alert.present();

  }
  private initsetpage(res: OSNotificationPayload){
    if(!this.platform.is('ios')){
      if(parseInt(res.actionButtons[0].id) < 4)
        this.setpage('MyExecutorTaskPage');
      else
        this.setpage('MyTaskPage');
    }
  }
  private setpage(page:any, params:any=null){
    let a:NavController=this.app.getActiveNav();
    a.setRoot(page, params);
  }
  getid(){
    return this.oneSignal.getIds().then((data)=>{
      return data.userId;
    });
  }
  getstatus():Promise<boolean>{
    return this.oneSignal.getPermissionSubscriptionState().then( value =>{
      console.log(value);
      return  value.subscriptionStatus.subscribed;
    }).catch( error =>{
      throw error;
    })
  }
  setpush(flag:boolean){
    this.oneSignal.setSubscription(flag);
  }
  setpushparam(el:PushElement):Promise<AnswerData | AnswerDataFalse>{
    let url=`User/Notification/Set`;
    return this.api.post(url, { status:el.value ? 1: 0, type:el.id}).then( value =>{
      return value;
    }).catch( error =>{
      return error;
    });
  }
  /**
   * получить параметры уведомлений
   */
  getpushparam(id:number[]):Promise<AnswerDataFalse | AnswerDataPush>{
    let url=`User/Notification/Get`;
    return this.api.post(url,{}).then( value =>{
      if(value.status){
        let temparray=<DataPush[]>value.data;
        let ar:DataPush[]=[];
        let flag:number;
        for(let i=0; i<id.length; i++){
          flag=1;
          for(let j=0; j<temparray.length; j++)
            if(temparray[j].ref_type_notification == id[i])
              flag=0
          ar.push({
            ref_type_notification:id[i],
            is_active:flag
          });
        }
        value.data=ar;
      }
      return value;
    }).catch( error =>{
      return error;
    });
    /*return new Promise( resolve =>{
      let ar:DataPush[]=[];
      for(let i=0; i<id.length; i++)
        ar.push({
          ref_type_notification:id[i],
          is_active:1
        });
      resolve({status:true, data:ar});
    });*/
  }

}
export interface PushElement{
  /**
   * ид пуша
   */
  id:number;
  /**
   * названия типа уведомлений
   */
  name:string;
  /**
   * true - включено false - выключено
   */
  value:boolean;
}