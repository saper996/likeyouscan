import { Injectable } from '@angular/core';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { Platform, ToastController, Alert, AlertController, PopoverController } from 'ionic-angular';
import { Http } from '@angular/http';
import { AlertOptions } from 'ionic-angular/components/alert/alert-options';
import { TypesHelp } from '../../pages/popover-help/popover-help';
@Injectable()
export class My_functions {
  font_size:any;
  private toast_flag:boolean=true;
  private text_toast:string='';
  private toastmessage:any;
  private toasttimeout:any;

  constructor(public http: Http, public platform:Platform, public alertCtrl:AlertController, public screen:ScreenOrientation, private toast:ToastController, public popover:PopoverController) {
  }
  /**
     * фунция вычисления ремов
     */
    private formula(){
      let height:number = this.platform.height();
      let html=document.getElementById('htmlapp');
      console.log(getComputedStyle(html).content);
      html.style.fontSize="";
      this.font_size=height/33.35;
      return this.font_size+'px';
  }
  lockorinentation(){
    if(this.platform.is('cordova'))
      {
        this.screen.lock('portrait');
      }
      let html=document.getElementById('htmlapp');
      html.setAttribute('style', 'font-size: '+this.formula()+' !important');
      this.screen.onChange().subscribe( () =>{
        let html=document.getElementById('htmlapp');
        html.setAttribute('style', 'font-size: '+this.formula()+' !important');
      });
      
  }
  toastmenu(text:string, duration:number, postion:string){
    if(!this.toast_flag && this.text_toast != text)
    {
        this.toastclose();
    }
    this.text_toast=text;
    if(this.toast_flag)
    {
        this.toast_flag=false;
        this.toastmessage = this.toast.create({
            message: text,
            position: postion,
            cssClass: 'messageclass'
        });
        this.toastmessage.present();
        this.toasttimeout=setTimeout(()=> {
            this.toastmessage.dismiss();
            this.toast_flag=true;
        }, duration);
    }
}
  toastclose(){
        if(!this.toast_flag){
            clearTimeout(this.toasttimeout);
            this.toastmessage.dismiss();
            this.toast_flag=true;
            
        }
    }
    alert4create():Alert
    /**
     * Создать алерт 4 версии
     * @param alert параметры алекрта
     */
    alert4create(alert:AlertOptions):Alert
    alert4create(alert?:AlertOptions, show:boolean=true):Alert{
        if(alert == null)
            alert={
                message:'Упс, что-то пошло не так',
                buttons:['OK']
            }
        if(alert.mode == null)
            alert.mode='ios';
        if(alert.cssClass == null){
            if(alert.title != null)
                alert.cssClass='alert4 alert4_head';
            else
                alert.cssClass='alert4';
        }
        if(alert.buttons == null || alert.buttons.length == 0){
            alert.buttons=[ 'OK' ];
        }
        let al=this.alertCtrl.create(alert);
        if(show){
            al.present();
            return al;
        }
        else
            return al;
    } 
    helpPopover(type:TypesHelp)
    helpPopover(type:TypesHelp, param:any)
    helpPopover(type:TypesHelp, param:any={}){
        if(param != null){
            param.type=type;
        }
        let temp=this.popover.create('PopoverHelpPage', param,{ cssClass: 'help_popover'} );
        temp.present();
    }
}

