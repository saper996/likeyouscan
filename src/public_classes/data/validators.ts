import { ValidatorFn, FormControl, Validators, ValidationErrors, AbstractControl } from "../../../node_modules/@angular/forms";

export class MyValidators{
    static cardvalidator() { 
        return new FormControl('', [Validators.compose([Validators.pattern('[0-9]{16}'), Validators.required])]);
    }
    static perevodvalidator(min:number=0,max:number=10000){ 
        return new FormControl('', Validators.compose([Validators.required, Validators.min(min), Validators.max(max)]));
    }
    static mobilevalidator (require:boolean=true){ 
        if(require)
            return new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]{10}')]));
        else
            return new FormControl('', Validators.compose([Validators.pattern('[0-9]{10}')]));
            //return new FormControl('', Validators.compose([Validators.pattern('^(\d{10}|)$')]));
    }
    static yandexmoney (){ 
        return new FormControl('', Validators.compose([Validators.required, Validators.pattern('[0-9]{10,20}')]));
    }
    static emailvalidator (){ 
        return new FormControl('', Validators.compose([Validators.required, Validators.email]));
    }
    static passwordvalidator(){
        return new FormControl( '', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(25), MyFuncValidator.passwordvalidator]));
    }
    static loginvalidator(){
        return new FormControl( '', Validators.compose([Validators.required, Validators.minLength(6), Validators.maxLength(20)]));
    }
    static FIOvalidator(){
        return new FormControl( '', Validators.compose([Validators.required, Validators.minLength(6)]));
    }
    static price(min:number, max?:number){
        if(max == null)
            return new FormControl( '', Validators.compose([Validators.required, Validators.min(min)]));
        else
            return new FormControl( '', Validators.compose([Validators.required, Validators.min(min), Validators.max(max)]));
    }
}
export class MyFuncValidator{
    static passwordvalidator:ValidatorFn=(control:AbstractControl)=>{
        let text:string=control.value
        let flagsmallsymbol=false;
        let flagbigsymbol=false;
        let numbersymbol=false;
        if(control.value == null)
            return { 'field is empty':true };
        for(let i=0; i<text.length && !(flagbigsymbol && flagsmallsymbol && numbersymbol); i++){
            if(!isNaN(Number(text[i])))
                numbersymbol=true;
            else if(text[i] == text[i].toUpperCase())
                flagbigsymbol=true;
            else if(text[i] == text[i].toLowerCase())
                flagsmallsymbol=true;
        }
        let error:string[]=[];
        if(!flagbigsymbol)
            error.push('not have big symbol');
        if(!flagsmallsymbol)
            error.push('not have small symbol');
        if(!numbersymbol)
            error.push('not have numeric');
        if(error.length == 0)
            return null;
        else{
            let temp:ValidationErrors={
            }
            for(let i=0; i<error.length; i++){
                temp[error[i]]=true;
            }
            return temp;
        }
    }
}