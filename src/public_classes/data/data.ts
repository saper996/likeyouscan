/**
 * объект из запроса
 */
export interface AnswerData{
    /**
     * успешный или нет запрос
     */
    status:boolean;
    /**
     * массив с данными или объект
     */
    data:any;
}
export interface Photo{
    /**
     * Путь к картинке
     */
    file_name:string;
    /**
     * высота изображения
     */
    height?:number;
    /**
     * ширина изображения
     */
    width?:number;
    /**
     * ид изоражения
     */
    id_image:number;
    latitude?:number;
    longitude?:number;
    /**
     * кол-во лайков
     */
    count?:number;
    /**
     * номер площадки
     */
    id_area?:number;
}
export interface AnswerDataFalse extends AnswerData{
    data:{
        messages:string[];
    }
}
export interface DataPush{
    is_active:number;
    ref_type_notification:number;
}
export interface AnswerDataPush extends AnswerData{
    data:DataPush[];
}
export interface Company{
    emblem:Photo;
    id_company:number;
    name:string;
    ref_emblem?:number;
}
export interface AnswerDataT<T> extends AnswerData{
    data:T;
}
export interface AnswerDataArrayT<T> extends AnswerData{
    data:T[];
}
export interface TypePromo{
    comment:string;
    id_type_of_promo:number;
}
export interface Infodate{
    date:string;
    time:string;
}
export interface TaskPromo{
    idCompany:number;
    company?:Company;
    header:string;
    description_executor:string;
    description_buyer:string;
    count:number;
    budget:number;
    isPercent:boolean;
    date_start:Infodate;
    date_end:Infodate;
    type:TypePromo;
    id_task_promo?:number;
    ref_status?:Statuses;
    status_flag?:boolean;
    ref_type_of_promo:number;
    products:Product[];
}
export enum Statuses{
    zero,
    /**
     * Новое
     */
    new,
    /**
     * Осмотр модератором
     */
    check,
    /**
     * Ожидает загрузки, начала задания
     */
    wait,
    /**
     * Активное, 
     */
    active,
    /**
     * сhecked
     */
    checked,
    /**
     * завершено
     */
    end,
    /**
     * отклонено
     */
    denied
}
export interface Product{
    description:string;
    idImage:number;
    picture?:string;
    image?:Photo;
    filepicture?:string;
    progress?:number;
}