import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Deeplinks } from '@ionic-native/deeplinks';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { Camera } from '@ionic-native/camera';
import { InfoPageModule } from '../pages/info/info.module';
import { AuthorizationPageModule } from '../pages/authorization/authorization.module';
import { Api } from '../providers/api';
import { My_functions } from '../providers/function/functions';
import { ComponentsModule } from '../components/components.module';
import { HttpModule } from '@angular/http';
import { Network } from '@ionic-native/network';
import { File } from '@ionic-native/file';
import { IonicStorageModule } from '@ionic/storage';
import { OneSignal } from '@ionic-native/onesignal';
import { ScreenOrientation } from '@ionic-native/screen-orientation';
import { FileTransfer } from '@ionic-native/file-transfer';
import { InAppBrowser } from '@ionic-native/in-app-browser';
import { RegistrationPageModule } from '../pages/registration/registration.module';
import { StartingPageModule } from '../pages/starting/starting.module';
import { LostPasPageModule } from '../pages/lost-pas/lost-pas.module';
import { MyCompanyPageModule } from '../pages/my-company/my-company.module';
import { AddCompanyPageModule } from '../pages/add-company/add-company.module';
import { PopoverHelpPageModule } from '../pages/popover-help/popover-help.module';

@NgModule({
  declarations: [
    MyApp,
    HomePage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    InfoPageModule, ComponentsModule,
    AuthorizationPageModule,HttpModule,IonicStorageModule.forRoot(),
    RegistrationPageModule,StartingPageModule,LostPasPageModule,MyCompanyPageModule,AddCompanyPageModule,PopoverHelpPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    Deeplinks,
    QRScanner,
    Camera,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    Api,Network,File,OneSignal,ScreenOrientation,FileTransfer,InAppBrowser,
    My_functions
  ]
})
export class AppModule {}
