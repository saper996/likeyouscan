import { Component, ViewChild } from '@angular/core';
import { Platform, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HomePage } from '../pages/home/home';
import { Deeplinks } from '@ionic-native/deeplinks';
import { My_functions } from '../providers/function/functions';
import { StartingPage } from '../pages/starting/starting';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = StartingPage;
  @ViewChild(Nav) nav: Nav;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, deep:Deeplinks,my_func:My_functions) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      my_func.lockorinentation();
      splashScreen.hide();
      deep.routeWithNavController(this.nav, {
        '/': HomePage,
        '/register_workers': HomePage
      }).subscribe(match => { 
          // match.$route - the route we matched, which is the matched entry from the arguments to route()
          // match.$args - the args passed in the link
          // match.$link - the full link data
          console.log('Successfully matched route', match);
        }, nomatch => {
          // nomatch.$link - the full link data
          console.error('Got a deeplink that didn\'t match', nomatch);
        });
    });
  }
}

